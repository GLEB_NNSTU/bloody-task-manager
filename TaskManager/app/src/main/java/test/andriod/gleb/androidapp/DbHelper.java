package test.andriod.gleb.androidapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by glebn on 12/19/2015.
 */
public class DbHelper extends SQLiteOpenHelper {

    public static final class DbAttr {
        public static final String DB_NAME = "TaskManager.db";
        public static final int DB_VERSION = 1;

        public static final String TABLE_NAME_TASK = "task";

        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_MESSAGE = "message";
    }


    public DbHelper(Context context) {
        super(context, DbAttr.DB_NAME, null, DbAttr.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "
                +   DbAttr.TABLE_NAME_TASK + " ("
                +   DbAttr.COLUMN_NAME_ID + " integer primary key autoincrement,"
                +   DbAttr.COLUMN_NAME_MESSAGE + " text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}
