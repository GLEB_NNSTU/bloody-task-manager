package test.andriod.gleb.androidapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by glebn on 12/20/2015.
 */
public class EditActivity extends Activity implements Button.OnClickListener{
    private EditText eEdit;
    private Button bSave;
    private ItemData itemData;
    private DbProvider dbProvider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        dbProvider = new DbProvider(this);

        bSave = (Button) findViewById(R.id.bSave);
        bSave.setOnClickListener(this);

        eEdit = (EditText) findViewById(R.id.eEdit);

        Bundle extra = getIntent().getExtras();
        if(extra != null) {
            int id = extra.getInt("EXTRA_ITEM_ID");
            itemData = dbProvider.getMessage(id);
            eEdit.setText(itemData.message);
        }
    }

    @Override
    public void onClick(View v) {
        itemData.message = eEdit.getText().toString();
        dbProvider.update(itemData);
        finish();
    }
}
