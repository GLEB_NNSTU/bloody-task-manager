package test.andriod.gleb.androidapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener, ListView.OnItemLongClickListener{

    private ListView taskList;
    private Button button;
    private static ArrayAdapter<String> adapter;
    private static ArrayList<String> arrayList;
    public static ArrayList<ItemData> arrayItem;

    private DbProvider dbProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbProvider = new DbProvider(this);

        // Setting ListView env
        taskList = (ListView) findViewById(R.id.listView);
        arrayList = new ArrayList<>();
        adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, arrayList);
        taskList.setAdapter(adapter);

        button = (Button) findViewById(R.id.button);

        refreshList();

        taskList.setOnItemClickListener(this);
        taskList.setOnItemLongClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle menu item selection
        switch (item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(this,AddActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public void forceRefresh(View v) {
        refreshList();
    }

    private void refreshList() {
        arrayList.clear();
        arrayItem = dbProvider.getMessages();
        for(int i = 0; i < arrayItem.size(); ++i) {
            arrayList.add(arrayItem.get(i).toString());
        }
        adapter.notifyDataSetChanged();
    }

    public void myToast(String msg) {
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra("EXTRA_ITEM_ID", arrayItem.get(position).id);
        startActivity(intent);
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        myToast("Deleting : " + arrayItem.get(position).id);
        dbProvider.remove(arrayItem.get(position).id);
        refreshList();
        return true;
    }
}
