package test.andriod.gleb.androidapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

/**
 * Created by glebn on 12/19/2015.
 */

public class AddActivity extends AppCompatActivity implements Button.OnClickListener{

    private EditText eTask;
    private Button bAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        bAdd = (Button) findViewById(R.id.bAdd);
        bAdd.setOnClickListener(this);

        eTask = (EditText) findViewById(R.id.eTask);
    }

    @Override
    public void onClick(View v) {
        DbProvider dbProvider = new DbProvider(this);
        dbProvider.add(eTask.getText().toString());
        finish();
    }
}
