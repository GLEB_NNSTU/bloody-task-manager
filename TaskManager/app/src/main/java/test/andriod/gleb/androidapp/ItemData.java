package test.andriod.gleb.androidapp;

/**
 * Created by glebn on 12/20/2015.
 */

public class ItemData {
    public String message;
    public int id;

    public ItemData(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public String toString() {
        return message;
    }
}
