package test.andriod.gleb.androidapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by glebn on 12/19/2015.
 */
public class DbProvider extends DbHelper {

    private DbHelper dbHelper;
    private SQLiteDatabase db;

    public DbProvider(Context context) {
        super(context);
        dbHelper = new DbHelper(context);
    }

    // Insert new data in DB
    public long add(String message) {
        db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DbAttr.COLUMN_NAME_MESSAGE, message);

        return db.insert(DbAttr.TABLE_NAME_TASK, null, values);
    }

    // Get a List of all messages in DB
    public ArrayList<ItemData> getMessages() {
        ArrayList<ItemData> arrayList = new ArrayList<>();

        db = dbHelper.getReadableDatabase();
        String[] projection = {DbAttr.COLUMN_NAME_ID, DbAttr.COLUMN_NAME_MESSAGE};
        Cursor cursor = db.query(DbAttr.TABLE_NAME_TASK, projection, null, null, null, null, null);

        arrayList.clear();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            ItemData temp = new ItemData(cursor.getInt(cursor.getColumnIndex(DbAttr.COLUMN_NAME_ID)),
                                         cursor.getString(cursor.getColumnIndex(DbAttr.COLUMN_NAME_MESSAGE)));
            arrayList.add(temp);
            cursor.moveToNext();
        }
        cursor.close();
        return arrayList;
    }

    public ItemData getMessage(int position) {
        // TODO : create normal query to get only one row.
        db = dbHelper.getReadableDatabase();
        String[] projection = {DbAttr.COLUMN_NAME_ID, DbAttr.COLUMN_NAME_MESSAGE};
        Cursor cursor = db.rawQuery("select "
                                    + DbAttr.COLUMN_NAME_ID + "," + DbAttr.COLUMN_NAME_MESSAGE
                                    + " from " + DbAttr.TABLE_NAME_TASK + " where "
                                    + " id=" + position, null);
        cursor.moveToFirst();
        ItemData itemData = new ItemData(cursor.getInt(cursor.getColumnIndex(DbAttr.COLUMN_NAME_ID)),
                cursor.getString(cursor.getColumnIndex(DbAttr.COLUMN_NAME_MESSAGE)));

        cursor.close();
        return itemData;
    }

    public void remove(int index) {
        db = dbHelper.getWritableDatabase();
        String selection = DbAttr.COLUMN_NAME_ID + " = " + index;
        db.delete(DbAttr.TABLE_NAME_TASK, selection, null);
    }

    public void update(ItemData itemData) {
        db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DbAttr.COLUMN_NAME_MESSAGE, itemData.message);
        String condition = "id=" + itemData.id;

        db.update(DbAttr.TABLE_NAME_TASK, values, condition, null);
    }
}
